const fs = require("fs");
const {execSync} = require("child_process");

const notificationData = require("./data.json");

// avoid using ssh protocol for unintended changes in the repo
const repoUrl = "https://hg.adblockplus.org/notifications";
const repoTempDirectory = "notifications";

function validateData() {
  if (notificationData.sampleGroups.reduce((a, b) => a + b, 0) !== 1)
    throw Error("sample groups should add up to 1");
}

function createNotificationFile(activeGroups = []) {
  const content =
    getHeaderFragment(activeGroups) + getGroupsFragment(activeGroups);
  fs.writeFileSync(
    `${repoTempDirectory}/${notificationData.fileName}`,
    content,
  );
}

function getHeaderFragment(activeGroups) {
  const inactive = !activeGroups.length;

  let fragment =
    `inactive = ${inactive}\n` +
    `severity = newtab\n\n` +
    `title.en-US = ${notificationData.title}\n` +
    `message.en-US = ${notificationData.title}\n` +
    `links = ${notificationData.links}\n\n`;

  notificationData.targets.forEach((target) => {
    fragment += `target = ${target.join(" ")}\n`;
  });

  fragment += "\n";

  return fragment;
}

function getGroupsFragment(activeGroups) {
  let fragment = "";

  notificationData.sampleGroups.forEach((group, idx) => {
    fragment += `[${idx + 1}]\n` + `sample = ${group.toFixed(2)}\n`;

    if (activeGroups.includes(idx + 1)) {
      fragment +=
        `title.en-US = ${notificationData.title}\n` +
        `message.en-US = ${notificationData.title}\n`;
    }

    if (idx !== notificationData.sampleGroups.length - 1) {
      fragment += "\n";
    }
  });

  return fragment;
}

function createPatch(activeGroups, idx, pastActiveGroups) {
  let deactivatedPercentage = 0;
  pastActiveGroups.forEach((pastActiveGroup) => {
    if (!activeGroups.includes(pastActiveGroup)) {
      deactivatedPercentage += parseInt(
        notificationData.sampleGroups[pastActiveGroup - 1] * 100,
        10,
      );
    }
  });

  let activatedPercentage = 0;
  activeGroups.forEach((activeGroup) => {
    if (!pastActiveGroups.includes(activeGroup)) {
      activatedPercentage += parseInt(
        notificationData.sampleGroups[activeGroup - 1] * 100,
        10,
      );
    }
  });

  let exposurePercentage = activeGroups.reduce(
    (a, b) => a + parseInt(notificationData.sampleGroups[b - 1] * 100, 10),
    0,
  );

  const deactivateText = deactivatedPercentage
    ? `deactivate ${deactivatedPercentage}%`
    : "";
  const activateText = activatedPercentage
    ? `activate ${activatedPercentage}%`
    : "";

  let commitMssgBody =
    [deactivateText, activateText].join(
      `${deactivateText && activateText ? " and " : ""}`,
    ) + ` of users to reach ${exposurePercentage}%`;

  commitMssgBody = commitMssgBody[0].toUpperCase() + commitMssgBody.slice(1);

  const commitMessage = `${notificationData.commitPrefix}${commitMssgBody}`;

  const patchFileIndex = `${idx + 1}`.padStart(
    `${notificationData.rolloutSteps.length}`.length,
    "0",
  );
  const patchFileName = `${patchFileIndex}_${[
    deactivatedPercentage ? `deactivate-${deactivatedPercentage}` : "",
    activatedPercentage ? `activate-${activatedPercentage}` : "",
  ].join(
    `${deactivatedPercentage && activatedPercentage ? "-" : ""}`,
  )}-reach-${exposurePercentage}.patch`;

  execSync(`
    cd ${repoTempDirectory} && hg add &&
    hg commit -m "${commitMessage}" &&
    hg export > ../patches/${patchFileName}
  `);
}

function cleanTemporalFiles() {
  fs.rmdir(`./${repoTempDirectory}`, {recursive: true}, (err) => {
    if (err) {
      throw err;
    }
    console.log("temporal files sucessfully removed");
  });
}


validateData();

console.log(`cloning repo ${repoUrl}`);
execSync(
  `rm -rf ${repoTempDirectory} && hg clone ${repoUrl} ${repoTempDirectory}`,
);
console.log("repo sucessfully cloned");

const parsedRolloutSteps = notificationData.rolloutSteps.map((step) =>
  step === "" ? [] : step.split("-").map((item) => parseInt(item, 10)),
);

console.log("creating patches");
execSync("rm -rf patches && mkdir -p patches");
parsedRolloutSteps.forEach((activeGroups, idx) => {
  createNotificationFile(activeGroups);
  const pastActiveGroups = parsedRolloutSteps[idx - 1] || [];
  createPatch(activeGroups, idx, pastActiveGroups);
});
console.log("patches successfully generated");

cleanTemporalFiles();
