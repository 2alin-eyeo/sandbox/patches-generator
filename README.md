## Introduction

Script to automate the generation of patches that represent the different states of the notification file of a donation campaign. Based on the donation campaigns [wiki article](https://gitlab.com/eyeo/adblockplus/abpui/adblockplusui/-/wikis/donation-campaigns) that describes the format of such notification file.

## Instructions

Fill `data.json` with the desired valued (see bellow) and run `node generate-patches.js`.

## Filling data.json

#### `fileName`

Name of the notification file.

**example:** `"10"`
#### `commitPrefix`

Prefix issued for the commit message generation.

**example:** `"Issue #1020 - "`

#### `title`

Notification ID, defined as `AdblockplusDonateYYYYMM`.

**example:** `"AdblockplusDonate202109"`

#### `links`

Link that the notification will open in  a new tab.

**example:** `"https://new.adblockplus.org/%LANG%/update?an=%ADDON_NAME%&av=%ADDON_VERSION%&ap=%APPLICATION_NAME%&apv=%APPLICATION_VERSION%&p=%PLATFORM_NAME%&pv=%PLATFORM_VERSION%"`

#### `targets`

An array of arrays that describes the notification's target user base, in the shape strings (target descriptor). Valid target descriptors satisfy the following regex (see [`parser.py`](https://gitlab.com/eyeo/devops/legacy/sitescripts/-/blob/master/sitescripts/notifications/parser.py)):
- `^(extension|application|platform)=.+$`
- `^(extension|application|platform)Version(=|\>=|\<=).+$`
- `^blockedTotal(=|\>=|\<=)\d+$`
- `^locales=[\w\-,]+$`

**example:** 
```
[
  [
    "extension=adblockpluschrome",
    "extensionVersion>=3.11.1",
    "locales=ar,am,bg,bn,ca,cs,da,el,en,en-CA,en-GB,en-US,es,es-419,et,fa,fi,fil,gu,he,hi,hr,hu,id,it,ja,kn,ko,lt,lv,ml,mr,ms,nl,no,pl,pt-BR,pt-PT,ro,ru,sk,sl,sr,sv,sw,ta,te,th,tr,uk,vi,zh-CN,zh-TW"
  ],
  [
    "extension=adblockplusfirefox",
    "extensionVersion>=3.11.1",
    "locales=ar,am,bg,bn,ca,cs,da,el,en,en-CA,en-GB,en-US,es,es-419,et,fa,fi,fil,gu,he,hi,hr,hu,id,it,ja,kn,ko,lt,lv,ml,mr,ms,nl,no,pl,pt-BR,pt-PT,ro,ru,sk,sl,sr,sv,sw,ta,te,th,tr,uk,vi,zh-CN,zh-TW"
  ]
]
```

#### `sampleGroups`

An array of values between 0 and 1 that represents user base partitions and that add up to 1.

**example:** `[0.01, 0.14, 0.15, 0.2, 0.2, 0.1, 0.2]`

#### `rolloutSteps`

**example:** `[
    "1",
    "1-2",
    "1-2-3",
    "1-2-3-4",
    "1-2-3-4-5",
    "2-3-4-5",
    "3-4-5-6",
    "4-5-6-7",
    "5-6-7",
    "6-7",
    "7",
    ""
  ]`
